# README #

Get ready for the PMA "Golden Cow" developer challenge!

This repo contains some notes and sample scripts for getting started with
voice recognition and voice output on the Raspberry Pi device.

# Initial OS setup

Connect the Raspberry pi to the power supply. This will start it up.
Plug in a monitor via HDMI and a keyboard+mouse via USB.
Connect an Ethernet cable or connect to one of the available WIFI networks.

When the raspberry pi starts up for the first time, there should be a menu with
possible operating systems to install. The instructions below should work when
you choose the Raspbian OS - the default/recommended Linux distribution for
the Raspberry pi.

# Test the connected peripherals

Once Raspbian is installed, the following steps can be used to test if the
microphone and speakers are working properly.

## Check microphone connection

The notes for this section and the next one were taken from the following page
[best-voice-recognition-software-for-raspberry-pi](https://diyhacking.com/best-voice-recognition-software-for-raspberry-pi/)
The steps for PiAUISuite did not however work on first try, but the page does
explain nicely how to set up and test the microphone connection.

Unplug the USB microphone and run the following command in the terminal:
```
lsusb
```
It should list system devices.

Next, plug in the USB microphone again, and run the same command again:
```
lsusb
```

Verify that there is now one more item in the list of system devices.

## Adjust microphone volume

Use the following command in the terminal:
```
alsamixer
```
A UI will appear that shows the microphone recording volume.
Use the up arrow key to increase the volume, close to 100%. Press the Esc key
to exit the program

## Try a recording

Try recording using the microphone using the following command:
```
arecord -D plughw:1,0 --format cd test.wav
```
Press Ctrl-C to stop recording. Check if a test.wav file got created.

To play the test.wav file on the speakers, run the following command:

```
aplay test.wav
```

Note: When connecting a monitor via HDMI, the audio output will be sent to the
connected HDMI device. If the monitor has speakers, you might not need to
connect separate speakers. If the monitor does not have speakers, try to
connect your external speakers to the monitor's audio output jack.


# Voice output

## Installation

Run the following commands in the terminal to install the necessary programs to
translate any given text into voice output through the speakers
(Note: these commands were taken from the page
[Speech recognition](https://elinux.org/RPi_Text_to_Speech_(Speech_Synthesis)))

```
sudo apt-get update
sudo apt-get upgrade
```
Note: the last command can take a long time (possibly an hour).
Try to run these commands first, in the background while planning/preparing
other steps of the project ;-)

Then run the following commands:

```
sudo apt-get install alsa-utils
```

Open the file /etc/modules in a text editor.
You can use the Text Editor program from the Raspbian start menu, or use a 
terminal editor like vi or nano. For example, to edit using vi, use:

```
sudo vi /etc/modules
```

and add the following line to the /etc/modules file:

```
snd_bcm2835
```

Run the following command to install the mplayer audio/movie player:

```
sudo apt-get install mplayer
```

Edit the config file /etc/mplayer/mplayer.conf using a text editor. For example 
using the vi terminal editor:

```
sudo vi /etc/mplayer/mplayer.conf
```

and add the following line to the file:

```
nolirc=yes
```

## Test voice output
In this repo's `samples` directory, there's a speech.sh file that can be used
to test the voice output.

Make this script executable and run it with english text:

```
cd samples
chmod u+x speech.sh
./speech.sh Say something interesting over the microphone
```


That means, given a piece of text, e.g. "It is a nice day today", send the voice
output to the device's speakers.

# Voice Input

## Setup Google API

We can use the [Google Cloud platform's speech API](https://cloud.google.com/speech)
For this to work, we need to register an account, create a project and billing
information. When signing up, the initial usage is for free ($300 usage credits).

For the Golden Cow challenge, we can use Guenther's account.

To use the API, you'll need to place a file with the account's private key.
You can use [this one](https://pma.space/files/googlecloud/sa2-f4cbd4f96a48.json)

On the Raspberry pi, you can use the following command to create a GoldenCow
directory and download the private key file there:

```
mkdir -p /home/pi/GoldenCow
cd /home/pi/GoldenCow
wget https://pma.space/files/googlecloud/sa2-f4cbd4f96a48.json
```

Next, define an environment variable named GOOGLE_APPLICATION_CREDENTIALS with
the path to the downloaded sa2-f4cbd4f96a48.json file.

For the above example, the private key would be at /home/pi/GoldenCow/sa2-f4cbd4f96a48.json
Therefore, add the following line to the /home/pi/.bashrc file using an editor:

```
export GOOGLE_APPLICATION_CREDENTIALS="/home/pi/GoldenCow/sa2-f4cbd4f96a48.json"
```

Close the terminal window and open a new one, so that the
GOOGLE_APPLICATION_CREDENTIALS environment variable is availble in the terminal
shell.

## Install Google API and dependencies

Run the following command in the terminal to install the Python Google Cloud
Speech API:

```
pip install --upgrade google-cloud-speech
```

Run the following commands to install ffmpeg:

```
sudo apt-get update --fix-missing
sudo apt-get install ffmpeg
```

Run the following commands to install pyaudio:

```
sudo apt-get install portaudio19-dev
pip install pyaudio
```

## Try out voice recognition

From the `samples` directory of this repo, try running the `googlespeech.py`
Python script. It will start recording. Press Ctrl-C to end the recording.
If everything works, it should print the text as recognized from the recording.

Note: speak loud, clear and in english ;-)

```
python googlespeech.py
```

## Try transcribing a stream of microphone audio

From the `samples` directory of this repo, try running the `transcribe_streaming_mic.py` Python script.

```
python transcribe_streaming_mic.py
```

It will start recording. Speak english words and it will print the words as it
understands them. 

Note: The microphone recording is limited to 60 seconds.

## More samples

The two samples in this repo (googlespeech.py and transcribe_streaming_mic.py)
are based on two sample files from the
[Google Cloud Samples](https://github.com/GoogleCloudPlatform/python-docs-samples.git)
repository. Clone this repo to get more sample files.



